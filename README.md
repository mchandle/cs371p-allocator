# CS371p: Object-Oriented Programming Collatz Repo

* Name: Miles Chandler

* EID: mac9325

* GitLab ID: mchandle

* HackerRank ID: mchandler1

* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)

* GitLab Pipelines: https://gitlab.com/mchandle/cs371p-allocator/pipelines

* Estimated completion time: 16

* Actual completion time: (actual time in hours, int or float)

* Comments: (any additional comments you have)
